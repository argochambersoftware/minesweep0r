/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argochamber.game.vintage.textsweeper.controllers;

/**
 * This helps us to control the chrono thingy.
 * @author Pablo
 */
public class ChronoWrapper extends Thread {
    
    private cat.vallbona.cronometre.CountDown chrono;
    
    public ChronoWrapper(float secs) {
        int mins = (int)(secs/60.0f);
        int sec = (int)(((secs/60.0f) - (float)mins) * 60.0f);
        this.chrono = new cat.vallbona.cronometre.CountDown(mins, sec);
        this.chrono.setVisible(true);
        this.chrono.setTitle("Countdown");
        this.chrono.setGameEndMessage("Time Up!");
    }

    @Override
    public void run() {
        this.chrono.run();
        this.chrono.dispose();
    }

    /**
     * Starts the chronometer.
     */
    public void startCountdown() {
        chrono.iniciarCronometre();
    }
    
    /**
     * Stops the chronometer.
     */
    public synchronized void stopCountdown() {
        chrono.pararCronometre();
    }

    /**
     * Returns true if the chrono hasn't ended the countdown yet.
     * @return 
     */
    public synchronized boolean isChronoAlive() {
        return chrono.isCronometreActiu();
    }

    /**
     * Sets the message that pops when the chrono ends.
     * @param msg 
     */
    public void setGameEndMessage(String msg) {
        chrono.setGameEndMessage(msg);
    }
    
    
    
}
