package com.argochamber.game.vintage.textsweeper.controllers;

import com.argochamber.game.vintage.textsweeper.Main;
import com.argochamber.game.vintage.textsweeper.game.GameMineSweeper;

import com.argochamber.game.vintage.textsweeper.game.Player;
import com.argochamber.game.vintage.textsweeper.views.Console;
import com.argochamber.game.vintage.textsweeper.views.ConsoleDecorator;
import java.util.ArrayList;

/**
 * The main game controller.
 *
 * @author sigmasoldi3r
 *
 */
public class Controller {

    /**
     * The interval between move and move in animations.
     */
    public static final int MENU_INTRO_INTERVAL = 160;

    /**
     * The view.
     */
    private Console console;

    /**
     * The decorator.
     */
    private ConsoleDecorator decorator;

    /**
     * Maks the mainframe
     */
    private void makeFrame() {
        decorator.printFrame(0, 0, Main.CONSOLE_ROWS, Main.CONSOLE_COLUMNS, ' ');
    }

    /**
     * There to start the game's logic.
     *
     * @throws java.lang.InterruptedException
     */
    public void begin() throws InterruptedException {

        for (int i = 16; i > 0; i--) {
            console.clear(false);
            decorator.printTitle(1, i);
            console.flush();
            Thread.sleep(MENU_INTRO_INTERVAL);
        }

        console.clear(false);
        decorator.printTitle(1, 1);
        console.flush();
        Thread.sleep(200);

        makeFrame();
        decorator.printTitle(1, 1);
        decorator.printMenu(2, 8);
        console.flush();

        System.out.print("Choose one: ");
        int n = console.getInt(1, 3);

        while (n == 2) {
            decorator.help();
            makeFrame();
            decorator.printTitle(1, 1);
            decorator.printMenu(2, 8);
            console.flush();
            System.out.print("Choose one: ");
            n = console.getInt(1, 3);
        }

        if (n == 1) {
            makeFrame();
            decorator.printTitleSmall(1, 3);
            decorator.printMenu(2, 8);
            console.flush();
            Thread.sleep(1000);

            for (int i = 3; i >= 0; i--) {
                console.clear(false);
                makeFrame();
                decorator.printTitleSmall(1, i);
                console.flush();
                Thread.sleep(MENU_INTRO_INTERVAL);
            }

            Thread.sleep(500);

            console.clear(false);
            makeFrame();
            decorator.printTitleSmall(1, 0);
            decorator.printSizeChart(1, 4);
            console.flush();

            System.out.println("Enter size: ");
            Integer[] dimensions = console.getInts(2);

            //The player's list.
            ArrayList<Player> players = new ArrayList<>();
            players.add(new Player("Player"));

            //Start the game.
            GameMineSweeper game = new GameMineSweeper(dimensions[0], dimensions[1], players);
            game.setController(this);
            game.begin();
            
            game.getChrono().join();
        }

        //Game's end
        makeFrame();
        decorator.printTitle(1, 0);
        decorator.printBye(2, 6);
        console.flush();
        System.out.println("Game by Pablo \"sigmasoldier\" Blanco. 2016");

    }

    /**
     * @return the console
     */
    public Console getConsole() {
        return console;
    }

    /**
     * @param console the console to set
     */
    public void setConsole(Console console) {
        this.console = console;
    }

    /**
     *
     * @return
     */
    public ConsoleDecorator getDecorator() {
        return decorator;
    }

    /**
     *
     * @param decorator
     */
    public void setDecorator(ConsoleDecorator decorator) {
        this.decorator = decorator;
    }

}
