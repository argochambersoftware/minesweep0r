/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argochamber.game.vintage.textsweeper.controllers;

import com.argochamber.game.vintage.textsweeper.game.Game;
import com.argochamber.game.vintage.textsweeper.views.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is the main clock.
 * <p>
 * Basically it will tick time until it runs out, then the game will end!
 * </p>
 * @author Pablo
 * @deprecated Due to dificulty to refresh console's buffer without flickering.
 */
public class Clock extends Thread {

    private final Console out;
    private final Game game;
    private boolean exit;
    private float seconds;
    
    public Clock(Console out, Game game, int seconds) {
        this.out = out;
        this.exit = false;
        this.game = game;
        this.seconds = seconds;
    }
    
    /**
     * Ends the clock.
     */
    public void exit(){
        this.exit = true;
    }
    
    @Override
    public void run() {
        while (!exit){
            synchronized(this.out){
                
                int hours = (int) seconds / 3600;
                int remainder = (int) seconds - hours * 3600;
                int mins = remainder / 60;
                remainder = remainder - mins * 60;
                int secs = remainder;
                
                if (seconds % 1 == 0){
                    this.out.setCursor(80, 4);
                    this.out.print("Time: "+mins+":"+secs);
                    this.out.flush();
                }
                
                seconds -= 0.1f;
                if (seconds <= 0){
                    this.exit();
                    this.game.end();
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Clock.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    
    
}
