/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argochamber.game.vintage.textsweeper.game;

import com.argochamber.game.vintage.textsweeper.Main;
import com.argochamber.game.vintage.textsweeper.controllers.ChronoWrapper;
import com.argochamber.game.vintage.textsweeper.controllers.Controller;
import com.argochamber.game.vintage.textsweeper.game.math.Vector;
import com.argochamber.game.vintage.textsweeper.thirdparty.FastNoise;
import java.util.Arrays;
import java.util.List;

/**
 * <h1>The game controller.</h1>
 * <p>
 * This class defines the rules of the game.
 * </p>
 *
 * @author Pablo
 */
public class GameMineSweeper extends Game {

    /**
     * This is the graphics set.
     */
    public static final Character[] TERRAIN = new Character[3];

    static {
        TERRAIN[Player.TYPE_MINE] = '#';
        TERRAIN[Player.TYPE_STAR] = '+';
        TERRAIN[Player.TYPE_BLANK] = ' ';
    }

    /**
     * Aesthetic purposes only.
     */
    public static final String[] PLAYER_FRAME = {
        "|",
        "|",
        "|"
    };

    /**
     * Non-discovered tile.
     */
    public static final Character NO_TILE = '~';

    /**
     * Min/Max values for the map.
     */
    public static final int MIN = 0, MAX = 2, SPACING = 3,
            MAX_HEIGHT = 24,
            MAX_WIDTH = 41;

    /**
     * Factors.
     */
    public static final float TIME_FACTOR = 0.4f;

    /**
     * Bit masks.
     */
    public static final byte MASK_TYPES = 0b11,
            MASK_UNRAVEL = 0b100,
            MASK_META = 0b1111000,
            TYPE_MINE = 2,
            TYPE_STAR = 1,
            TYPE_BLANK = 0,
            FLAG_TRUNCATED = 0b000010,
            FLAG_CHEATS = 0b000100,
            FLAG_UNKNOWN_COMMAND = 0b001000,
            FLAG_NO_SUCH_COORDINATES = 0b010000,
            FLAG_ALREADY_UNRAVELED = 0b100000;

    /**
     * Octaves for perlin noise.
     */
    public static final int MAP_OCTAVES = 6;

    /**
     * Command list.
     */
    public static final String //-- Action Command --//
            COMMAND_HELP = "help",
            COMMAND_HELP_SHORTCUT = "h",
            COMMAND_CHEATS = "show me the money",
            COMMAND_DISABLE_CHEATS = "sweet regret",
            COMMAND_EXIT = "exit",
            COMMAND_EXIT_SHORTCUT = "x",
            COMMAND_QUIT = "quit",
            //-- Action commands --//
            ACTION_CIR = "cir",
            ACTION_HCIR = "hcir";

    /**
     * Controller's reference.
     */
    private Controller controller;

    /**
     * Holds the field data.
     */
    private byte[][] field;

    /**
     * Player list.
     */
    private List<Player> players;

    /**
     * The player that is playing at this time.
     */
    private int currentPlayer = 0;

    /**
     * Third party chronometer.
     */
    private ChronoWrapper chrono;

    /**
     * Initializes the game with the given parameters.
     *
     * @param width
     * @param height
     * @param players
     */
    public GameMineSweeper(int width, int height, List<Player> players) {
        this.players = players;
        int fwidth = width > MAX_WIDTH ? MAX_WIDTH : width;
        int fheight = height > MAX_HEIGHT ? MAX_HEIGHT : height;
        if (width > MAX_WIDTH || height > MAX_HEIGHT) {
            this.setFlag(FLAG_TRUNCATED, true);
        }
        this.field = new byte[fwidth][fheight];
    }

    @Override
    public void setup() {
        //Populate the field:
        for (int i = 0; i < this.field.length; i++) {
            for (int j = 0; j < this.field[0].length; j++) {
                int n = (int) (FastNoise.noise(i, j, MAP_OCTAVES) / 25.0f) - 4;
                n = n < MIN ? MIN : n;
                n = n > MAX ? MAX : n;
                this.field[i][j] = (byte) n;
            }
        }
        float secs = this.field.length * this.field[0].length * TIME_FACTOR;
        this.chrono = new ChronoWrapper(secs);
        this.chrono.start();
        this.chrono.setGameEndMessage("Time Up!");
    }

    /**
     * Prints a mine filed.
     *
     * @param field
     * @param x
     * @param y
     * @param mapset
     */
    public void printMineField(byte[][] field, int x, int y, Character[] mapset) {
        this.getController().getDecorator().printVerticalRuler(field[0].length, 1, x, y);
        this.getController().getDecorator().printHorizontalRuler(field.length, SPACING, x + 3, y - 1);
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                this.getController().getConsole().setCursor(i * SPACING + x + 3, j + y);
                Character ch;
                try {
                    byte val = field[i][j];
                    if ((val & MASK_UNRAVEL) == 0 && !this.getFlag(FLAG_CHEATS)) {
                        ch = NO_TILE;
                    } else {
                        ch = mapset[val & MASK_TYPES];
                    }
                } catch (ArrayIndexOutOfBoundsException outbounds) {
                    ch = '!';
                    this.getController().getConsole().setCursor(22, 1);
                    this.getController().getConsole().print("!: There are errors in the map render.");
                }
                this.getController().getConsole().print(ch.toString());
            }
        }
    }

    /**
     * Prints out the player's information.
     *
     * @param x
     * @param y
     * @param ply
     */
    public void printPlayer(int x, int y, Player ply) {
        this.getController().getConsole().setCursor(x, y - 1);
        this.getController().getConsole().print(PLAYER_FRAME);
        this.getController().getConsole().setCursor(x + 1, y);
        this.getController().getConsole().print("Player: @".concat(ply.getName()));
        this.getController().getConsole().setCursor(x + 1, y + 1);
        this.getController().getConsole().print("Stars: ".concat(((Integer) ply.getStars()).toString()));
        this.getController().getConsole().setCursor(x + 1, y + 2);
        this.getController().getConsole().print("Mines: ".concat(((Integer) ply.getMines()).toString()));
    }

    /**
     * Called whenever an unknown command has been issued to notify the player.
     */
    public void unknownCommand() {
        this.setFlag(FLAG_UNKNOWN_COMMAND, true);
    }

    /**
     * If there is not a coordinate existant, call this to make sure that the
     * player is aware.
     */
    public void outBounds() {
        this.setFlag(FLAG_NO_SUCH_COORDINATES, true);
    }

    /**
     * Notifies to the player that this place is already taken.
     */
    public void alreadyTaken() {
        this.setFlag(FLAG_ALREADY_UNRAVELED, true);
    }
    
    /**
     * Sets a material type in the volumetric point.
     * @param i
     * @param j
     * @param mat 
     */
    protected void set(int i, int j, byte mat){
        try {
            field[i][j] = mat;
        } catch (ArrayIndexOutOfBoundsException ex) {/*...*/}
    }
    
    /**
     * This will look for in the field if is a mine what is down there.
     *
     * @param i
     * @param j
     * @return
     */
    public boolean isMine(int i, int j) {
        try {
            return (field[i][j] & MASK_TYPES) == Player.TYPE_MINE;
        } catch (ArrayIndexOutOfBoundsException ex) {
            return false;
        }
    }

    /**
     * Sets the flag of unraveled to high.
     *
     * @param i
     * @param j
     */
    public void unravel(int i, int j) {
        try {
            field[i][j] |= MASK_UNRAVEL;
        } catch (ArrayIndexOutOfBoundsException ex) {/*...*/}
    }

    /**
     * Returns if the slot in the field is unraveled or not.
     *
     * @param i
     * @param j
     * @return
     * @throws ArrayIndexOutOfBoundsException
     */
    public boolean isUnraveled(int i, int j) {
        try {
            return (field[i][j] & MASK_UNRAVEL) != 0;
        } catch (ArrayIndexOutOfBoundsException ex) {
            return false;
        }
    }

    /**
     * This iterates until the propagation of fire has ended.
     */
    public void propagate() {
        //Backup the old
        byte[][] oldfield = new byte[this.field.length][this.field[0].length];

        //While the both models are different, continue computing.
        while (!Arrays.deepEquals(field, oldfield)) {
            for (int i = 0; i < oldfield.length; i++) {
                oldfield[i] = Arrays.copyOf(this.field[i], this.field[i].length);
            }
            //Compute a propagated model
            for (int i = 0; i < this.field.length; i++) {
                for (int j = 0; j < this.field[0].length; j++) {

                    if (isMine(i, j) && isUnraveled(i, j)) {
                        if (isMine(i + 1, j)) {
                            unravel(i + 1, j);
                        }
                        if (isMine(i - 1, j)) {
                            unravel(i - 1, j);
                        }
                        if (isMine(i, j + 1)) {
                            unravel(i, j + 1);
                        }
                        if (isMine(i, j - 1)) {
                            unravel(i, j - 1);
                        }
                    }
                }
                
            }
        }

    }

    /**
     * Tries to parse, true if number, false elsewhere.
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Returns true if the action can only be done by a cheat mode / admin
     * player.
     *
     * @param action
     * @return
     */
    public boolean isAdminAction(String action) {
        return action.equals(ACTION_CIR) | action.equals(ACTION_HCIR);
    }
    

    /**
     * This computes an action that is not a command (Like unraveling, shifting
     * map properties...).
     *
     * @param args
     */
    private void computeAction(String[] args) {

        //Ensure that the command list is not empty.
        if (args.length > 0) {

            //The action to do, or just the X Coord:
            String act = args[0].trim();
            if (isAdminAction(act) && this.getFlag(FLAG_CHEATS)) {
                if (act.equals(ACTION_CIR)) {
                    //For circles filling.
                    try {
                        int a = Integer.parseInt(args[1]);
                        int b = Integer.parseInt(args[2]);
                        int r = Integer.parseInt(args[3]);
                        byte mat = (byte)Integer.parseInt(args[4]);
                        
                        for (int i = 0; i < r*2; i++){
                            for (int j = 0; j < r*2; j++) {
                                Vector AB = new Vector(r,r).getAB(new Vector(i, j));
                                if (AB.length() <= r){
                                    set(a+i-r, b+j-r, mat);
                                }
                            }
                        }
                        
                    } catch (NumberFormatException ex) {
                        unknownCommand();
                    } catch (ArrayIndexOutOfBoundsException bounds) {
                        outBounds();
                    }
                } else if (act.equals(ACTION_HCIR)) {
                    //For circles filling.
                    try {
                        int a = Integer.parseInt(args[1]);
                        int b = Integer.parseInt(args[2]);
                        int r = Integer.parseInt(args[3]);
                        byte mat = (byte)Integer.parseInt(args[4]);
                        int wid = 1; //Optional parameter, the width of the wall.
                        try {
                            wid = Integer.parseInt(args[5]);
                        } catch (ArrayIndexOutOfBoundsException ex){}
                        
                        for (int i = 0; i < r*2; i++){
                            for (int j = 0; j < r*2; j++) {
                                Vector AB = new Vector(r,r).getAB(new Vector(i, j));
                                if (AB.length() <= r && AB.length() > (r-wid)){
                                    set(a+i-r, b+j-r, mat);
                                }
                            }
                        }
                        
                    } catch (NumberFormatException ex) {
                        unknownCommand();
                    } catch (ArrayIndexOutOfBoundsException bounds) {
                        outBounds();
                    }
                }
            } else if (isNumeric(act)) {
                //* ELSEWHERE IS A MOVEMENT *//
                if (args.length == 2) {
                    try {
                        int a = Integer.parseInt(act);
                        int b = Integer.parseInt(args[1]);
                        if (isUnraveled(a, b)) {
                            alreadyTaken();
                        } else {
                            //If you're at this stage, that means that you could move correctly.
                            unravel(a, b);
                            this.players.get(currentPlayer).score(field[a][b] & MASK_TYPES);
                            this.currentPlayer++;
                            this.currentPlayer = this.currentPlayer >= this.players.size() ? 0 : this.currentPlayer;
                            propagate();
                        }
                    } catch (NumberFormatException ex) {
                        unknownCommand();
                    } catch (ArrayIndexOutOfBoundsException bounds) {
                        outBounds();
                    }
                } else {
                    unknownCommand();
                }
            } else {
                unknownCommand();
            }
        } else {
            unknownCommand();
        }
    }

    /**
     * This loops the game.
     */
    @Override
    public void loop() {
        if (this.chrono.isAlive()) {
            this.getController().getConsole().clear(false);
            this.getController().getDecorator().printFrame(0, 0, Main.CONSOLE_ROWS, Main.CONSOLE_COLUMNS, ' ');
            this.getController().getDecorator().printTitleSmall(1, 0);
            this.printMineField(field, 2, 7, TERRAIN);
            this.printPlayer(80, 1, this.players.get(currentPlayer));
            if (this.getFlag(FLAG_TRUNCATED)) {
                this.getController().getConsole().setCursor(22, 2);
                this.getController().getConsole().print("!: The map has been truncated to fit screen.");
            }
            if (this.getFlag(FLAG_CHEATS)) {
                this.getController().getConsole().setCursor(22, 3);
                this.getController().getConsole().print("!: Cheat mode enabled.");
            }
            if (this.getFlag(FLAG_UNKNOWN_COMMAND)) {
                this.getController().getConsole().setCursor(22, 4);
                this.getController().getConsole().print("?: Unknown or malformed command entered.");
                this.setFlag(FLAG_UNKNOWN_COMMAND, false);
            } else if (this.getFlag(FLAG_NO_SUCH_COORDINATES)) {
                this.getController().getConsole().setCursor(22, 4);
                this.getController().getConsole().print("!: Coordinates entered out of bounds!");
                this.setFlag(FLAG_NO_SUCH_COORDINATES, false);
            } else if (this.getFlag(FLAG_ALREADY_UNRAVELED)) {
                this.getController().getConsole().setCursor(22, 4);
                this.getController().getConsole().print("!: This place has been already taken.");
                this.setFlag(FLAG_ALREADY_UNRAVELED, false);
            }
            this.getController().getConsole().flush();
            System.out.print("Issue a command: ");
            String cmd = this.getController().getConsole().getString();
            if (cmd.equalsIgnoreCase(COMMAND_HELP) || cmd.equalsIgnoreCase(COMMAND_HELP_SHORTCUT)) {
                // ------------- SHOW HELP ------------ //
                this.getController().getDecorator().help();
            } else if (cmd.equalsIgnoreCase(COMMAND_CHEATS)) {
                this.setFlag(FLAG_CHEATS, true);
            } else if (cmd.equalsIgnoreCase(COMMAND_DISABLE_CHEATS)) {
                this.setFlag(FLAG_CHEATS, false);
            } else if (cmd.equalsIgnoreCase(COMMAND_EXIT) || cmd.equalsIgnoreCase(COMMAND_EXIT_SHORTCUT) || cmd.equalsIgnoreCase(COMMAND_QUIT)) {
                System.out.print("Really want to quit? (YOU WILL LOOSE YOUR SCORE!) (Y/N) ");
                String conf = this.getController().getConsole().getString();
                if (conf.equalsIgnoreCase("y")) {
                    this.end();
                }
            } else {
                String[] strs = cmd.split(" ");
                computeAction(strs);
            }
        } else {
            this.end();
        }
        if (this.getFlag(Game.GAME_END)) {
            this.chrono.setGameEndMessage(" - Game end -");
            this.chrono.stopCountdown();
        }
    }

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public ChronoWrapper getChrono() {
        return chrono;
    }

}
