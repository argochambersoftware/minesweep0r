package com.argochamber.game.vintage.textsweeper.game;

/**
 * This exception is thrown when the number was not in the desired range.
 * @author Pablo
 */
public class NotInRangeException extends Exception {
    
}
