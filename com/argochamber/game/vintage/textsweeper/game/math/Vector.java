/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argochamber.game.vintage.textsweeper.game.math;

/**
 * Cartessian 2D vector
 * @author Pablo
 */
public class Vector {
    
    /**
     * Doubles
     */
    private Double x, y;
    
    /**
     * Constructor.
     * @param x
     * @param y 
     */
    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Computes pithagorean modulo.
     * @return 
     */
    public double length(){
        return Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2));
    }
    
    /**
     * Computes the vector that goes from A to B.
     * @param B
     * @return 
     */
    public Vector getAB(Vector B){
        return new Vector(x-B.x, y-B.y);
    }
    
    /**
     * Casts to a double array.
     * @return 
     */
    public double[] toArray(){
        return new double[]{x,y};
    }

    @Override
    public String toString() {
        return "Vector{" + "x=" + x + ", y=" + y + '}';
    }
    
}
