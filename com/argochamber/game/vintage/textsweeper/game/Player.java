package com.argochamber.game.vintage.textsweeper.game;

/**
 * Player data class.
 *
 * @author sigmasoldi3r
 *
 */
public class Player {

    /**
     * Delta score.
     */
    public static final int TYPE_STAR = 2,
            TYPE_MINE = 1,
            TYPE_BLANK = 0;

    /**
     * The player's nick name.
     */
    private String name;

    /**
     * The player's score.
     */
    private int mines, stars;

    /**
     * Public constructor.
     *
     * @param name
     */
    public Player(String name) {
        this.name = name;
        this.mines = 0;
        this.stars = 0;
    }

    /**
     * Rises the player's score.
     * @param type 
     */
    public void score(int type){
        if (type == TYPE_STAR){
            stars++;
        } else if (type == TYPE_MINE){
            mines++;
        }
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public int getMines() {
        return mines;
    }

    public int getStars() {
        return stars;
    }
    
}
