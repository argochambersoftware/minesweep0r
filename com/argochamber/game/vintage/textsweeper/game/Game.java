/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.argochamber.game.vintage.textsweeper.game;

/**
 * This is the abstract game descriptor.
 * @author Pablo
 */
public abstract class Game {
    
    /**
     * Game flags.
     */
    public static final byte GAME_END = 0b1;
    
    /**
     * Game status flags.
     */
    private byte state;
    
    /**
     * Default constructor.
     */
    public Game(){
        state = 0x00;
    }
    
    /***
     * Main game loop logic.
     */
    public abstract void loop();
    
    /**
     * Called just before the loop starts.
     * Only called once.
     */
    public abstract void setup();
    
    /**
     * Make the game start.
     */
    public final void begin(){
        this.setup();
        while(!this.isEnded()){
            this.loop();
        }
    }
    
    /**
     * Reads the game status flag.
     * @param flag
     * @return 
     */
    public final boolean getFlag(byte flag){
        return (this.state & flag) != 0;
    }
    
    /**
     * Set the flag to high or low.
     * @param flag
     * @param status 
     */
    public final void setFlag(byte flag, boolean status){
        this.state = (byte)(status ? this.state | flag : this.state & ~flag);
    }
    
    /**
     * Gets the status flagset.
     * @return 
     */
    public final byte getStatus(){
        return this.state;
    }
    
    /**
     * Ends the game.
     */
    public final void end(){
        this.setFlag(Game.GAME_END, true);
    }
    
    /**
     * Has the game ended yet?
     * @return 
     */
    public final boolean isEnded(){
        return this.getFlag(Game.GAME_END);
    }
}
