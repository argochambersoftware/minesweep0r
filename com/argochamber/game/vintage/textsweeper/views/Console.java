package com.argochamber.game.vintage.textsweeper.views;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;

import com.argochamber.game.vintage.textsweeper.controllers.Controller;
import com.argochamber.game.vintage.textsweeper.game.NotInRangeException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Console {

    /**
     * Scroll amount when flushing.
     */
    public static final int SCROLL_CLEAR = 16;

    /**
     * Scroll up
     */
    public static final byte[] BLANKS = new byte[SCROLL_CLEAR];

    static {
        Arrays.fill(BLANKS, (byte) '\n');
    }

    /**
     * Blank Character for alpha.
     * This will not be printed out.
     */
    public static final byte BLANK = '\0';

    /**
     * The view's reference to the controller.
     */
    private Controller controller;

    /**
     * The console's output stream
     */
    private PrintStream out = new PrintStream(System.out, false);
    
    /**
     * Console's user input.
     */
    private BufferedReader input = new BufferedReader( new InputStreamReader( System.in ) );

    /**
     * This is the internal buffer.
     */
    private byte[] buffer;

    /**
     * This will give the buffer's size.
     */
    private final int columns, rows;

    /**
     * Those are the pointer of the print.
     */
    private int x, y;

    /**
     * Public constructor.
     *
     * @param controller
     */
    public Console(Controller controller, int columns, int rows) {
        this.controller = (controller);
        this.buffer = new byte[columns * rows];
        this.columns = columns;
        this.rows = rows;
        Arrays.fill(this.buffer, (byte) ' ');
    }

    /**
     * This flushes the stream out.
     */
    public void flush() {
        try {
            this.out.write(BLANKS);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < rows; i++) {
            this.out.write(buffer, i * columns, columns);
            this.out.println();
        }
        this.out.flush();
    }

    /**
     * This clears the buffer.
     *
     * @param flush
     */
    public void clear(boolean flush) {
        Arrays.fill(this.buffer, (byte) ' ');
        if (flush) {
            this.out.flush();
        }
    }

    /**
     * Prints at the current pointer position.
     *
     * @param ch
     */
    public void print(String ch) {
        byte[] bytes = ch.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            if (bytes[i] != BLANK) {
                this.buffer[ this.x + i + this.y * this.columns ] = bytes[i];
            }
        }
    }
    
    /**
     * Prints at the current pointer position.
     * @param i 
     */
    public void print(Integer i){
        this.print(i.toString());
    }
    
    /**
     * Prints at the current pointer position.
     * @param i 
     */
    public void print(Byte i){
        this.print(i.toString());
    }
    
    /**
     * Prints at the current pointer position.
     * @param i 
     */
    public void print(Character i){
        this.print(i.toString());
    }
    
    /**
     * Prints at the current pointer position.
     * @param i 
     */
    public void print(Double i){
        this.print(i.toString());
    }
    
    /**
     * Prints at the current pointer position.
     * @param i 
     */
    public void print(Boolean i){
        this.print(i.toString());
    }
    
    /**
     * Prints at the current pointer position.
     * @param i 
     */
    public void print(Float i){
        this.print(i.toString());
    }
    
    /**
     * Prints a sequence of strings as if it were an image.
     * @param seq 
     */
    public void print(String[] seq){
        for (String s : seq) {
            if ((this.getY()+1) < this.rows){
                this.setCursor(this.getX(), this.getY() + 1);
                this.print(s);
            }
        }
    }
    
    /**
     * Reads an integer from the input stream.
     * @param from
     * @param to
     * @return 
     */
    public int getInt(Integer from, Integer to){
        Integer n = null;
        while (n == null){
            try {
                String line = input.readLine();
                if (!line.isEmpty()){
                    n = Integer.parseInt(line);
                    if ( (n < from || n > to) && from < to ){
                        n = null;
                        throw new NotInRangeException();
                    }
                }
            } catch (NumberFormatException nex){
                System.err.println("Only numbers allowed!");
            } catch (IOException e){
                System.err.println("Something went wrong, try again please.");
            } catch (NotInRangeException ex) {
                System.err.println("This value is not in range! ("
                        .concat(from.toString())
                        .concat(" - ")
                        .concat(to.toString())
                        .concat(")")
                );
            }
        }
        return n;
    }
    
    /**
     * All ranged format int.
     * @return 
     */
    public int getInt(){
        return this.getInt(0, -1);
    }
    
    /**
     * <strong>Gets many integers at once, from the same line.</strong>
     * <p>
     *  If only needed one, use getInt instead! Much faster!
     * </p>
     * @param size
     * @param separator
     * @return 
     */
    public Integer[] getInts(Integer size, String separator){
        Integer[] n = null;
        while (n == null){
            try {
                String[] lineData = input.readLine().split(separator);
                if (lineData.length > 0 && (lineData.length == size || size == 0)){
                    n = new Integer[lineData.length];
                    for (int i = 0; i < lineData.length; i++){
                        n[i] = Integer.parseInt(lineData[i]);
                    }
                } else {
                    throw new NotInRangeException();
                }
            } catch (NumberFormatException nex){
                System.err.println("Only numbers allowed!");
            } catch (IOException e){
                System.err.println("Something went wrong, try again please.");
            } catch (NotInRangeException ex) {
                System.err.println("Not in the specified range! (entries = "
                        .concat(size.toString())
                        .concat(")")
                );
            }
        }
        return n;
    }
    
    /**
     * Method delegator. Uses default separator.
     * @param n
     * @return 
     */
    public Integer[] getInts(int n){
        return this.getInts(n, " ");
    }
    
    /**
     * Retrieves any array of integers.
     * @return 
     */
    public Integer[] getInts(){
        return this.getInts(0);
    }
    
    /**
     * Returns the string from the console input.
     * @return 
     */
    public String getString(){
        String s = null;
        while (s == null){
            try {
                s = this.input.readLine();
            } catch (IOException ex) {
                //Silent retry.
            }
        }
        return s;
    }

    /**
     * @return the controller
     */
    public Controller getController() {
        return controller;
    }

    /**
     * @param controller the controller to set
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * @return the out
     */
    public PrintStream getOut() {
        return out;
    }

    /**
     * @param out the out to set
     */
    public void setOut(PrintStream out) {
        this.out = out;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x < this.columns ? x : this.columns - 1;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y < this.rows ? y : this.rows - 1;
    }

    /**
     * This sets the cursor position.
     *
     * @param x
     * @param y
     */
    public void setCursor(int x, int y) {
        this.x = x < this.columns ? x : this.columns - 1;
        this.y = y < this.rows ? y : this.rows - 1;
    }

    public BufferedReader getInput() {
        return input;
    }

    public void setInput(BufferedReader input) {
        this.input = input;
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }
    
    
}
