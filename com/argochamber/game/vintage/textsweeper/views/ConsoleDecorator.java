package com.argochamber.game.vintage.textsweeper.views;

import com.argochamber.game.vintage.textsweeper.Main;
import com.argochamber.game.vintage.textsweeper.game.GameMineSweeper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Those are some console utilities.
 *
 * @author sigmasoldi3r
 *
 */
public class ConsoleDecorator {

    public static final char DEFAULT_VERTICAL_PIPE = '|',
            DEFAULT_HORIZONTAL_PIPE = '-',
            DEFAULT_CROSS = '+',
            DEFAULT_UP_RIGHT_INTERSECT = '\\',
            DEFAULT_DOWN_RIGHT_INTERSECT = '/',
            DEFAULT_UP_LEFT_INTERSECT = '/',
            DEFAULT_DOWN_LEFT_INTERSECT = '\\';
    /*public static final char DEFAULT_VERTICAL_PIPE = '║',
            DEFAULT_HORIZONTAL_PIPE = '═',
            DEFAULT_CROSS = '╬',
            DEFAULT_UP_RIGHT_INTERSECT = '╗',
            DEFAULT_DOWN_RIGHT_INTERSECT = '╝',
            DEFAULT_UP_LEFT_INTERSECT = '╔',
            DEFAULT_DOWN_LEFT_INTERSECT = '╚';*/

    public static final String[] TITLE = {
        "   /'\\_/`\\  __                          ",
        "  /\\      \\/\\_\\    ___      __    ____  ",
        "  \\ \\ \\__\\ \\/\\ \\ /' _ `\\  /'__`\\ /',__\\ ",
        "   \\ \\ \\_/\\ \\ \\ \\/\\ \\/\\ \\/\\  __//\\__, `\\",
        "    \\ \\_\\  \\_\\ \\_\\ \\_\\ \\_\\ \\____\\/\\____/",
        "     \\/_/ \\/_/\\/_/\\/_/\\/_/\\/____/\\/___/ "
    },
            MENU_OPTIONS = {
                "#========================================#",
                "| @ 1) Singleplayer Game                 |",
                "| @ 2) Help                              |",
                "| @ 3) Exit                              |",
                "#========================================#"
            },
            BYE_CAPTION = {
                " ___                  _ ",
                "(  _`\\               ( )",
                "| (_) ) _   _    __  | |",
                "|  _ <'( ) ( ) /'__`\\| |",
                "| (_) )| (_) |(  ___/| |",
                "(____/'`\\__, |`\\____)(_)",
                "       ( )_| |       (_)",
                "       `\\___/'   "
            },
            TITLE_SMALL = {
                " _, _ _ _, _ __,  _,",
                " |\\/| | |\\ | |_  (_ ",
                " |  | | | \\| |   , )",
                " ~  ~ ~ ~  ~ ~~~  ~ "
            },
            SIZE_INPUT = {
                "#= Choose a size: =======#",
                "| H ^  |_|_|_|_          |",
                "| e |  |_|_|_|_          |",
                "| i |  |_|_|_|_          |",
                "| g |  |_|_|_|_          |",
                "| h |                    |",
                "| t +--------->          |",
                "|    w i d t h           |",
                "#========================#",
                " (IE: 5 6 )               "
            },
            HELP_TITLE = {
                ". . .-. .   .-. ",
                "|-| |-  |   |-' ",
                "' ` `-' `-' '  "
            },
            CHEAT_SHEET = {
                "#= CHEAT SHEET =============#",
                "|      -> Nothing (Water)   |",
                "|   +  -> Star (+1 Point)   |",
                "|   #  -> Mine (-2 points)  |",
                "|   ~  -> Water, to unravel |",
                "#===========================#"
            },
            COMMAND_SHEET = {
                "h/help               -> Shows this help.",
                "x/exit/quit          -> Exits the game.",
                "x y (ie: 5 7)        -> Touches the specified coordinates. When you touch a mine, the",
                "                  explosion propagates, if an star/blank is touched, score or nothing.",
                "CHEATS:",
                "show me the money    -> Enables the cheat mode.",
                "sweet regret         -> Disables the cheat mode.",
                "cir x y r mat        -> Fills a circle at x,y with a radius r and with material mat.",
                "hcir x y r mat [w]   -> Fills an outset circle, width(w) is optional, defaults to 1."
            };

    private char verticalPipe,
            horizontalPipe,
            cross,
            upRightIntersect,
            downRightIntersect,
            upLeftIntersect,
            downLeftIntersect;

    /**
     * Console printer reference.
     */
    private Console console;

    /**
     * Primary constructor.
     *
     * @param console
     */
    public ConsoleDecorator(Console console) {
        this.console = console;
        this.verticalPipe = DEFAULT_VERTICAL_PIPE;
        this.horizontalPipe = DEFAULT_HORIZONTAL_PIPE;
        this.cross = DEFAULT_CROSS;
        this.upRightIntersect = DEFAULT_UP_RIGHT_INTERSECT;
        this.upLeftIntersect = DEFAULT_UP_LEFT_INTERSECT;
        this.downLeftIntersect = DEFAULT_DOWN_LEFT_INTERSECT;
        this.downRightIntersect = DEFAULT_DOWN_RIGHT_INTERSECT;

    }

    /**
     * Prints the title.
     *
     * @param x
     * @param y
     */
    public void printTitle(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(TITLE);
    }
    
    /**
     * Prints the help title.
     *
     * @param x
     * @param y
     */
    public void printHelpTitle(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(HELP_TITLE);
    }
    
    /**
     * Prints the help title.
     *
     * @param x
     * @param y
     */
    public void printCommandSheet(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(COMMAND_SHEET);
    }
    
    /**
     * Prints the help title.
     *
     * @param x
     * @param y
     */
    public void printCheatSheet(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(CHEAT_SHEET);
    }
    
    /**
     * Prints the title in small.
     *
     * @param x
     * @param y
     */
    public void printTitleSmall(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(TITLE_SMALL);
    }

    /**
     * Prints the main menu options.
     *
     * @param x
     * @param y
     */
    public void printMenu(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(MENU_OPTIONS);
    }

    /**
     *
     * @param x
     * @param y
     */
    public void printSizeChart(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(SIZE_INPUT);
    }

    /**
     * Prints the "Bye!" message.
     *
     * @param x
     * @param y
     */
    public void printBye(int x, int y) {
        this.console.setCursor(x, y);
        this.console.print(BYE_CAPTION);
    }

    /**
     * Prints a frame of w*h at the top left corner of x y
     *
     * @param x
     * @param y
     * @param w
     * @param h
     * @param bg
     */
    public void printFrame(int x, int y, int w, int h, char bg) {
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (i == 0 && j == 0) {
                    // Top-left corner
                    this.console.setCursor(i + x, j + y);
                    this.console.print("" + this.upLeftIntersect);
                } else if (i == w - 1 && j == h - 1) {
                    // Down-Right corner
                    this.console.setCursor(i + x, j + y);
                    this.console.print("" + this.downRightIntersect);
                } else if (i == 0 && j == h - 1) {
                    this.console.setCursor(i + x, j + y);
                    this.console.print("" + this.downLeftIntersect);
                } else if (i == w - 1 && j == 0) {
                    this.console.setCursor(i + x, j + y);
                    this.console.print("" + this.upRightIntersect);
                } else if ((i != 0 && j == 0) || (i != 0 && j == h - 1)) {
                    // Horizontal piping
                    this.console.setCursor(i + x, j + y);
                    this.console.print("" + this.horizontalPipe);
                } else if ((i == 0 && j != 0) || (i == w - 1 && j != 0)) {
                    // Vertical piping
                    this.console.setCursor(i + x, j + y);
                    this.console.print("" + this.verticalPipe);
                } else {
                    this.console.setCursor(i + x, j + y);
                    this.console.print("" + bg);
                }
            }
        }
    }

    /**
     * Prints a vertical ruler.
     *
     * @param length
     * @param spacing
     * @param x
     * @param y
     */
    public void printVerticalRuler(Integer length, int spacing, int x, int y) {
        int digits = length.toString().length();
        for (Integer i = 0; i < length * spacing; i++) {
            int iDigits = i.toString().length();
            this.getConsole().setCursor(x, y + i);
            String chs = "";
            for (int j = 0; j < digits - iDigits; j++) {
                chs = chs.concat(" ");
            }
            if (i % spacing == 0) {
                chs = chs.concat(((Integer) (i / spacing)).toString());
            } else if (i % spacing == 1) {
                chs = chs.concat("_");
            }
            this.getConsole().print(chs.concat("|"));
        }
    }

    /**
     * Prints a vertical ruler.
     *
     * @param length
     * @param spacing
     * @param x
     * @param y
     */
    public void printHorizontalRuler(Integer length, int spacing, int x, int y) {
        for (Integer i = 0; i < length * spacing; i++) {
            String chs = "";

            if (i % spacing == 0) {
                chs = chs.concat(((Integer) (i / spacing)).toString());
                chs = chs.concat("|");
            }
            this.getConsole().setCursor(x + i - (((i / spacing) >= 10) ? 1 : 0), y);
            this.getConsole().print(chs);

        }
    }
    
    /**
     * This displays the help usage and cheatsheet.
     */
    public void help() {
        this.getConsole().clear(false);
        this.printFrame(0, 0, Main.CONSOLE_ROWS, Main.CONSOLE_COLUMNS, ' ');
        this.printTitleSmall(1, 0);
        this.printFrame(3, 6, (int)(Main.CONSOLE_ROWS*0.9), (int)(Main.CONSOLE_COLUMNS*0.7), '`');
        this.printHelpTitle(5, 4);
        this.printCheatSheet(5, 7);
        this.printFrame(5, 14, (int)(Main.CONSOLE_ROWS*0.7), (int)(Main.CONSOLE_COLUMNS*0.4), ' ');
        this.getConsole().setCursor(7, 14);
        this.getConsole().print("COMMANDS");
        this.printCommandSheet(7, 15);
        this.getConsole().flush();
        System.out.print("Press enter to resume...");
        try {
            this.getConsole().getInput().readLine();
        } catch (IOException ex) {
            Logger.getLogger(GameMineSweeper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Console reference getter.
     *
     * @return
     */
    public Console getConsole() {
        return console;
    }

    /**
     * Console reference setter.
     *
     * @param console
     */
    public void setConsole(Console console) {
        this.console = console;
    }

    /**
     * @return the verticalPipe
     */
    public char getVerticalPipe() {
        return verticalPipe;
    }

    /**
     * @param verticalPipe the verticalPipe to set
     */
    public void setVerticalPipe(char verticalPipe) {
        this.verticalPipe = verticalPipe;
    }

    /**
     * @return the horizontalPipe
     */
    public char getHorizontalPipe() {
        return horizontalPipe;
    }

    /**
     * @param horizontalPipe the horizontalPipe to set
     */
    public void setHorizontalPipe(char horizontalPipe) {
        this.horizontalPipe = horizontalPipe;
    }

    /**
     * @return the cross
     */
    public char getCross() {
        return cross;
    }

    /**
     * @param cross the cross to set
     */
    public void setCross(char cross) {
        this.cross = cross;
    }

    /**
     * @return the upRightIntersect
     */
    public char getUpRightIntersect() {
        return upRightIntersect;
    }

    /**
     * @param upRightIntersect the upRightIntersect to set
     */
    public void setUpRightIntersect(char upRightIntersect) {
        this.upRightIntersect = upRightIntersect;
    }

    /**
     * @return the downRightIntersect
     */
    public char getDownRightIntersect() {
        return downRightIntersect;
    }

    /**
     * @param downRightIntersect the downRightIntersect to set
     */
    public void setDownRightIntersect(char downRightIntersect) {
        this.downRightIntersect = downRightIntersect;
    }

    /**
     * @return the upLeftIntersect
     */
    public char getUpLeftIntersect() {
        return upLeftIntersect;
    }

    /**
     * @param upLeftIntersect the upLeftIntersect to set
     */
    public void setUpLeftIntersect(char upLeftIntersect) {
        this.upLeftIntersect = upLeftIntersect;
    }

    /**
     * @return the downLeftIntersect
     */
    public char getDownLeftIntersect() {
        return downLeftIntersect;
    }

    /**
     * @param downLeftIntersect the downLeftIntersect to set
     */
    public void setDownLeftIntersect(char downLeftIntersect) {
        this.downLeftIntersect = downLeftIntersect;
    }

}
