/**
 *
 */
package com.argochamber.game.vintage.textsweeper;


import com.argochamber.game.vintage.textsweeper.controllers.Controller;
import com.argochamber.game.vintage.textsweeper.views.Console;
import com.argochamber.game.vintage.textsweeper.views.ConsoleDecorator;

/**
 * @author sigmasoldi3r
 *
 */
public class Main {
    
    /**
     * Screen dimensions
     */
    public static final int CONSOLE_ROWS = 128, CONSOLE_COLUMNS = 32;
    
    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        
        //Instantiate the controller
        Controller controller = new Controller();

        //Instantiate the view
        Console console = new Console(controller, CONSOLE_ROWS, CONSOLE_COLUMNS);
        controller.setConsole(console);

        //This is the console's
        ConsoleDecorator decorator = new ConsoleDecorator(console);
        controller.setDecorator(decorator);
        
        //Make the controller start the game.
        controller.begin();
    }

}
