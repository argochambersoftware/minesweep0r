# Mine Sweeper
### Vintage Console Old Style Arcade
#### Argochamber Interactive 2016

* * *

Sweep out mines from the field!


* * *

## Command Spreadsheet

+ `show me the money` Enables the cheat mode.
+ `sweet regret` Disables the cheat mode.

#### Cheat mode commands
+ `cir x y r mat` Creates a circle of radius 'r', at 'x y' coordinates with material 'mat'.
+ `hcir x y r mat [w]` Creates a hollow circle of radius 'r', at 'x y' coordinates with material 'mat', of a width 'w'.

## Materials
+ `0` A blank (Renders to ' ').
+ `1` A Mine (Renders to '#').
+ `2` A star (Renders to '+').

*Note*: All materials render to '\~' if not in cheat mode and/or not unrevealed.

* * *

### TODO:

+ [ ] Add more fill options:
    + [x] Circle Fill
	+ [x] Hollow Circle Fill
	+ [ ] Square Fill
	+ [ ] Rectangle Fill
	+ [ ] Ellipse Fill
+ [ ] Add generation options:
    + [ ] Seed For generation
	+ [ ] More generator types
+ [ ] Add multiplayer